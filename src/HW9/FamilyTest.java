package HW9;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FamilyTest {

    private Family module;
/*
    @BeforeEach
    public void setUp() {
        Map<String,String> schedule = setUpDailySchedule();
        Woman mother = new Woman("Mother", "MotherLastName", 1979);
        Man father = new Man("Father", "FatherLastName", 1974);
        Human child = new Human("Child1", "ChildLastName", 2000);
        Human childToAdd = new Human("Child2","Child2Surname", 2019);
        Set<String> boriaHabits = new HashSet<>();
        List<Human> children = new ArrayList<>();
        Set<Pet> pets = new HashSet<>();
        children.add(child);
        boriaHabits.add("eat");
        boriaHabits.add("sleep");
        Pet catBoria = new DomesticCat("Boria",2, 46, boriaHabits);
        pets.add(catBoria);
        module = new Family(mother,father,children,pets);
    }

    @Test
    public void getMother(){
        String actual = module.getMother().toString();
        String expected = "Woman{name='Mother', surname='MotherLastName', year=1979, iq=0, schedule=null}";
        assertEquals(expected, actual);
    }

    @Test
    public void getFather(){
        module.getFather().setSchedule(setUpDailySchedule());
        String actual = module.getFather().toString();
        String expected = "Man{name='Father', surname='FatherLastName', year=1974, iq=0, schedule={WEDNESDAY=Meditate, MONDAY=Get Enough Sleep, THURSDAY=Workout, SUNDAY=Take Breaks To Re-energize, TUESDAY=Rise Early, FRIDAY=Eat A Good Breakfast, SATURDAY=Take A Nap}}";
        assertEquals(expected, actual);
    }

    @Test
    public void getChildren(){
        String actual = module.getChildren().get(0).toString();
        String expected = "Human{name='Child1', surname='ChildLastName', year=2000, iq=0, schedule=null}";
        assertEquals(expected, actual);
    }

    @Test
    public void getPets(){
        String actual = String.valueOf(module.getPets());
        String expected = "[DOMESTIC_CAT{nickname='Boria', age=2, trickLevel=46, habits=[sleep, eat]}]";
        assertEquals(expected, actual);
    }

    @Test
    public void deleteChildByExistingIndex(){
       Boolean actual = module.deleteChild(0);
       Boolean expected = true;
       assertEquals(expected,actual);
   }

    @Test
    public void deleteChildByWrongIndex(){
        Boolean actual = module.deleteChild(3);
        Boolean expected = false;
        assertEquals(expected,actual);
    }

    @Test
    public void deleteChildByExistingObject(){
        Boolean actual = module.deleteChild(module.getChildren().remove(0));
        Boolean expected = true;
        assertEquals(expected,actual);
    }

    @Test
    public void deleteChildByWrongObject(){
        Boolean actual = module.deleteChild(null);
        Boolean expected = false;
        assertEquals(expected,actual);
    }

    @Test
    public void countFamily(){
        int actual = module.countFamily();
        int expected = 3;
        assertEquals(expected,actual);
    }
*/

}
