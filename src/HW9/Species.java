package HW9;

public enum Species {
    DOMESTIC_CAT,
    DOG,
    ROBOCAT,
    PARROT,
    RABBIT,
    BIRD,
    FISH,
    UNKNOWN
}
