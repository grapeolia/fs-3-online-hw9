package HW9;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

public class FamilyController {

    private FamilyService familyService;

    public FamilyController(FamilyService familyService) {
        this.familyService = familyService;
    }

    public void start() {
        System.out.println("started");
        //petrenkoFamilyWithoutChildren, sydorenkoFamily, chumakFamily
        //System.out.println(familyService.getAllFamilies());
        familyService.displayAllFamilies();
        //System.out.println("Number of family members of [0] family: "+familyService.getAllFamilies().get(0).countFamilyMembers());
        //System.out.println("Number of family members of [1] family: "+familyService.getAllFamilies().get(1).countFamilyMembers());
        //System.out.println("Number of family members of [2] family: "+familyService.getAllFamilies().get(2).countFamilyMembers());

        //System.out.println(familyService.getFamiliesBiggerThan(3));
        //System.out.println(familyService.getFamiliesLessThan(3));
        //System.out.println(familyService.countFamiliesWithMemberNumber(2)+" families with 2 members");
        familyService.deleteFamilyByIndex(3);
        familyService.deleteFamilyByIndex(2);
        familyService.displayAllFamilies();
        familyService.bornChild(familyService.getFamilyById(0),"Ania","Dima");
        AdoptiveChild adoptiveChild = new AdoptiveChild("Child1","Chumak","10/03/2017", 90);
        AdoptiveChild adoptiveChildWithIncorrectDobFormat = new AdoptiveChild("Child1","Chumak","aa/03/2017", -3);
        System.out.println(adoptiveChildWithIncorrectDobFormat);
        familyService.adoptChild(familyService.getFamilyById(0), adoptiveChild);
        System.out.println(familyService.getFamilyById(1));
        familyService.bornChild(familyService.getFamilyById(1),"Katia","Sasha");
        familyService.bornChild(familyService.getFamilyById(1),"Katia","Sasha");
        familyService.deleteAllChildrenOlderThan(10);
        familyService.displayAllFamilies();
        familyService.deleteAllChildrenOlderThan(12);
        familyService.createNewFamily(new Woman(),new Man());
        System.out.println(familyService.count());
        familyService.getFamilyById(1);
        System.out.println(familyService.getPets(1).toString());
        familyService.addPet(0,new DomesticCat());
    }

}
