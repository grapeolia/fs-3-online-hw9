package HW9;

import java.util.Set;

public class DomesticCat extends Pet implements Pest{
    public DomesticCat() {
        setSpecies();
    }

    public DomesticCat(String nickname) {
        super(nickname);
        setSpecies();

    }

    public DomesticCat(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
        setSpecies();

    }

    @Override
    Species getSpecies() {
        return this.species;
    }

    public void setSpecies(){
        this.species = Species.DOMESTIC_CAT;
    }

    @Override
    public void foul() {
        System.out.println("I need to cover my tracks");
    }

    @Override
    public void respond() {
        System.out.println("Hi, owner! I'm "+this.nickname+". I miss you.");
    }
}
