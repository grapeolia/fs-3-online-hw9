package HW9;

import java.util.Objects;
import java.util.Set;

public abstract class Pet {
    public Species species;
    public String nickname;
    private int age;
    private int trickLevel;
    private Set<String> habits;
    public Pet() {
    }

    public Pet(String nickname) {
        this.nickname = nickname;
        this.species = Species.UNKNOWN;
    }

    public Pet(String nickname, int age, int trickLevel, Set<String> habits) {
        this.nickname = nickname;
        setAge(age);
        setTrickLevel(trickLevel);
        this.habits = habits;
        this.species = Species.UNKNOWN;

    }

    abstract Species getSpecies();

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        if(age >= 0 && age <= 30){
            this.age = age;
        } else {
            System.out.println("Age must be number 0...30. Age 0 has been set by default.");
            this.age = 0;
        }
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public void setTrickLevel(int trickLevel) {
        if(trickLevel>=0 && trickLevel <=100){
            this.trickLevel = trickLevel;
        } else {
            System.out.println("Trick Level must be number 0...100. Trick Level 0 has been set by default.");
            this.trickLevel = 0;
        }
    }

    public Set<String> getHabits() {
        return habits;
    }

    public void setHabits(Set<String> habits) {
        this.habits = habits;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Pet pet)) return false;
        return age == pet.age && trickLevel == pet.trickLevel && species == pet.species && Objects.equals(nickname, pet.nickname) && Objects.equals(habits, pet.habits);
    }

    @Override
    public int hashCode() {
        return Objects.hash(species, nickname, age, trickLevel, habits);
    }

    public void eat (){
        System.out.println("I'm eating");
    }

    public abstract void respond ();

    @Override
    public String toString() {
        return this.getSpecies() +"{" +
                "nickname='" + nickname + '\'' +
                ", age=" + age +
                ", trickLevel=" + trickLevel +
                ", habits=" + habits +
                '}';
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println(new StringBuilder().append("You're about to finalize ").append(this).toString());
        super.finalize();
    }
}
