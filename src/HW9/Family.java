package HW9;


import java.util.*;
import java.util.stream.Collectors;

public class Family {
    public Woman mother;
    public Man father;
    public List<Human> children;
    public Set<Pet> pets;

    public Family() {
        System.out.println("Parameters Mother & Father are must. Mother & Father are created by default. You can replace them by using set methods.");
        this.mother = new Woman();
        this.father = new Man();
    }
    public Family(Woman mother, Man father) {
        this.mother = mother;
        this.father = father;
    }

    public Family(Woman mother, Man father, List<Human> children) {
        this.mother = mother;
        this.father = father;
        this.children = children;
    }

    public Family(Woman mother, Man father, List<Human> children, Set<Pet> pets) {
        this.mother = mother;
        this.father = father;
        this.children = children;
        this.pets = pets;
    }

    public Woman getMother() {
        return mother;
    }

    public void setMother(Woman mother) {
        this.mother = mother;
    }

    public Man getFather() {
        return father;
    }

    public void setFather(Man father) {
        this.father = father;
    }

    public List<Human> getChildren() {
        return children;
    }

    public void setChildren(List<Human> children) {
        this.children = children;
    }

    public Set<Pet> getPets() {
        return pets;
    }

    public void setPets(Set<Pet> pets) {
        this.pets = pets;
    }

    @Override
    public String toString() {
        String motherRecord = mother != null ? mother.toString() : "no name mentioned";
        String fatherRecord = father != null ? father.toString() : "no name mentioned";
        String petRecord = pets != null ? String.valueOf(pets) : "no record";

        return "Family{" +
                "mother=" + motherRecord +
                ", father=" + fatherRecord +
                ", children=" + children +
                ", pet=" + petRecord +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Family family)) return false;
        return Objects.equals(mother, family.mother) && Objects.equals(father, family.father) && Objects.equals(children, family.children) && Objects.equals(pets, family.pets);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mother, father, children, pets);
    }

    public void addChild(Human newChild){
        if(getChildren() == null){
            setChildren(List.of(newChild));
        }else{
            setChildren(new ArrayList<>(getChildren()));
            getChildren().add(newChild);
        }
    }
    public boolean deleteChild(int childIndex){
        if (getChildren() != null
                && getChildren().size()>childIndex
                && getChildren().get(childIndex) != null) {
            setChildren(new ArrayList<>(getChildren()));
            getChildren().remove(childIndex);
            return true;
        }
        return false;
    }

    public boolean deleteChild(Human childToDelete) {
        if (getChildren() != null
                && getChildren().size()>0
                && getChildren().contains(childToDelete)) {
            setChildren(new ArrayList<>(getChildren()));
            getChildren().remove(childToDelete);
            return true;
        }
        return false;
    }

    public void addPet(Pet newPet){
        if(pets == null) {
            pets = new HashSet<>();
        }
        pets.add(newPet);
    }

    public boolean deletePet(Pet petToDelete) {
        if (getPets() != null
                && getPets().size()>0
                && getPets().contains(petToDelete)) {
            getPets().remove(petToDelete);
            return true;
        }
        return false;
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println(new StringBuilder().append("You're about to finalize ").append(this).toString());
        super.finalize();
    }

    public int countFamilyMembers(){
            int countPets = getPets() != null
                    ? getPets().size()
                    : 0;
            int countChildren = getChildren() != null
                    ? getChildren().size()
                    : 0;
            return 2+countPets+countChildren;
        }
    }
