package HW9;

import java.util.*;
import java.util.stream.Collectors;

public class CollectionFamilyDao implements FamilyDao {
   public List<Family> families;

    public CollectionFamilyDao(List<Family> families) {
        this.families = families;
    }

    public List<Family> getFamilies() {
        return families;
    }

    public void setFamilies(List<Family> families) {
        this.families = families;
    }

    @Override
    public List<Family> getAllFamilies() {
        return families;
    }

    @Override
    public Family getFamilyByIndex(int familyIndex) {
        if(families != null
                && familyIndex>=0
                && families.size()>familyIndex){
            return families.get(familyIndex);
        }
        return null;
    }

    @Override
    public boolean deleteFamily(int familyIndex) {
        if(families != null
                && familyIndex>=0
                && families.size()>familyIndex){
            families.remove(familyIndex);
            return true;
        }
        return false;
    }

    @Override
    public void saveFamily(Family newFamily) {
        if(families != null){
            for (Family family : families) {
                if (newFamily.equals(family)) {
                    System.out.println("Family already exist.");
                    return;
                }
            }
            } else {
            families = new ArrayList<>();
            }
        families.add(newFamily);
    }

    @Override
    public void addChild(Family familyForChild, Human newChild) {
        boolean isFamilyOnList = families != null && families.contains(familyForChild);
        if(isFamilyOnList) familyForChild.addChild(newChild);

        }

    @Override
    public void deleteChild(Family familyToDeleteChild, Human child) {
        familyToDeleteChild.deleteChild(child);
    }

    @Override
    public List<Human> getAllChildren() {
        List<Human> allChildren = new ArrayList<>();
        if(families != null){
            for(Family family : families){
                if(family.getChildren()!=null){
                    allChildren.addAll(family.getChildren());
                }
            }
        }
        return allChildren;
    }
    @Override
    public void deleteAllChildrenOlderThan(int minAge){
        if (this.getAllChildren() != null) {
            this.getAllChildren()
                    //.removeIf(child -> child.getYear() > minAge)
            ;
        }
    }

    @Override
    public int count() {
       return families!=null ? families.size() : 0;
    }

    @Override
    public Set<Pet> getPets(int familyIndex) {
        if(families != null && families.size()>familyIndex && families.get(familyIndex).getPets()!=null){
            return families.get(familyIndex).getPets();
        }
        return null;
    }

    @Override
    public void addPet(int familyIndex, Pet newPet) {
        if(families != null
                && families.size()>familyIndex){
            families.get(familyIndex).addPet(newPet);
        }
    }
}

