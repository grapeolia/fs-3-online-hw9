package HW9;

import java.time.*;
import java.util.Date;
import java.util.Map;
import java.util.Objects;

public class Human {
    private Family family;
    public String name;
    public String surname;
    public long birthDate;
    public int iq;
    private Map<String,String> schedule;

    public Human() {

    }

    public Human(String name, String surname, long birthDate) {
        this.name = name;
        this.surname = surname;
        setBirthDate(birthDate);
    }

    public Human(String name, String surname, long birthDate, int iq, Map<String,String> schedule) {
        this.name = name;
        this.surname = surname;
        setBirthDate(birthDate);
        setIq(iq);
        this.schedule = schedule;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }
    public long getBirthDate() {
        return this.birthDate;
    }

    public void setBirthDate(long birthDate) {
        //-2208988740000 - Date and time (GMT): Monday, January 1, 1900 12:01:00 AM
        if(birthDate>-2208985200000L){
            this.birthDate = birthDate;
        } else {
            System.out.println("BirthDate has incorrect value. Min value is -2208985200000L (01/01/1900). Value 0 (01/01/1970) has been set by default.");
            this.birthDate=0L;
        }
    }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        if(iq>=0 && iq<=100){
            this.iq = iq;
        } else {
            System.out.println("IQ has incorrect value. Must be 0..100. IQ 0 has been set by default.");
            this.iq = 0;
        }
    }

    public Family getFamily(){
        return family;
    }
    public Human getMother() {
       if(this.getFamily()!=null) {
           for(Human human : this.getFamily().getChildren()){
               if(this == human){
                   return this.getFamily().getMother();
               }
           }
       }
       System.out.println("I'm not a child in this family.");
       return null;
    }

    public Human getFather() {
        if(this.getFamily()!=null) {
            for(Human human : this.getFamily().getChildren()){
                if(this == human){
                    return this.getFamily().getFather();
                }
            }
        }
        System.out.println("I'm not a child in this family.");
        return null;
    }
    public Map<String,String> getSchedule() {
        return schedule;
    }

    public void setSchedule(Map<String,String> schedule) {
        this.schedule = schedule;
    }

    public void greetPets(){
        if (this.getFamily() != null && this.getFamily().getPets() != null) {
            String nickNames = "";
            for(Pet pet : this.getFamily().getPets()){
                if(pet.getNickname() != null && !Objects.equals(pet.getNickname(), "")) {
                    nickNames = ", " + pet.getNickname();
                }
            }
            System.out.println("Hi, " + nickNames + "!");
        } else {
            System.out.println("I don't have any pet. Or my pet doesn't have any nickname.");
        }
    }
    public void describePet(Pet myPet){
        if (this.getFamily().getPets() != null && this.getFamily().getPets().contains(myPet)) {
            for (Pet pet : this.family.getPets()) {
                if(pet == myPet){
                    String trickDescription = pet.getTrickLevel()>50
                            ? "very tricky"
                            : "barely tricky";
                    System.out.println("I've got a "+pet.getSpecies()+
                            ". It's "+pet.getAge()+
                            ", it's "+trickDescription+".");
                }
            }
        } else {
            System.out.println("I don't have this pet.");
        }
    }

    public String describeAge() {
        LocalDate humanDataOfBirthLocalDate =
                Instant.ofEpochMilli(this.getBirthDate()).atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate currentLocalDate =
                Instant.ofEpochMilli(System.currentTimeMillis()).atZone(ZoneId.systemDefault()).toLocalDate();
        Period ageOfHuman = Period.between(humanDataOfBirthLocalDate,currentLocalDate);
        int yearsAge = ageOfHuman.getYears();
        int monthsAge = ageOfHuman.getMonths();
        int days = ageOfHuman.getDays();
        return "I'm "+yearsAge+" years, "+monthsAge+" months, "+days+" days.";
    }


    @Override
    public String toString() {
        Date humanDOB = new Date(this.getBirthDate());
        LocalDate humanDataOfBirthLocalDate =
                Instant.ofEpochMilli(this.getBirthDate()).atZone(ZoneId.systemDefault()).toLocalDate();

        int yearOfBirth = humanDataOfBirthLocalDate.getYear();
        int monthOfBirth = humanDOB.getMonth()+1;
        int dayOfBirth = humanDataOfBirthLocalDate.getDayOfMonth();

        String motherRecord = (family != null && family.getMother() != null)
                ? (family.getMother().getName()+" "+ family.getMother().getSurname())
                : "no record";
        String fatherRecord = (family != null && family.getFather() != null)
                ? (family.getFather().getName() + " " + family.getFather().getSurname())
                : "no record";

        return "Human{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", DOB=" +dayOfBirth+"/"+monthOfBirth+"/"+yearOfBirth+
                ", iq=" + iq +
                ", schedule=" + schedule +
                '}';
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println(new StringBuilder().append("You're about to finalize ").append(this).toString());
        super.finalize();
    }
}
