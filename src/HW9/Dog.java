package HW9;

import java.util.Set;

public class Dog extends Pet implements Pest{
    Species species = Species.DOG;
    public Dog() {
        setSpecies();
    }

    public Dog(String nickname) {
        super(nickname);
        setSpecies();
    }

    public Dog(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
        setSpecies();
    }

    @Override
    public Species getSpecies(){
        return this.species;
    }

    public void setSpecies(){
        this.species = Species.DOG;
    }
    @Override
    public void foul() {
        System.out.println("I need to cover my tracks");
    }

    @Override
    public void respond() {
        System.out.println("Hi, owner! I'm "+this.nickname+". I miss you.");
    }
}
