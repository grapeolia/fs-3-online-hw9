package HW9;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.lang.reflect.Array;
import java.text.ParseException;
import java.util.*;

import static HW9.HappyFamily.setUpDailySchedule;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class FamilyServiceTest {
    private FamilyService module;

    @BeforeEach
    public void setUp(){

        Set<String> petHabits = Set.of("eat","sleep","jump");
        Pet simpleDomesticCat = new DomesticCat();
        Pet fullDog = new Dog("Tofik",2,34,petHabits);


        Man petrenkoFather = new Man("Father", "Petrenko", 1974);
        Woman petrenkoMother = new Woman("Mother", "Petrenko", 1979,78,setUpDailySchedule());
        Family petrenkoFamilyWithoutChildren = new Family(petrenkoMother,petrenkoFather);

        Man sydorenkoFather = new Man("Father", "Sydorenko", 1974);
        Woman sydorenkoMother = new Woman("Mother", "Sydorenko", 1979,78,setUpDailySchedule());
        Human sydorenkoBoyChild = new Human("childBoy","Sydorenko",2004);
        List<Human> sydorenkoChildren = List.of(sydorenkoBoyChild);
        Set<Pet> sydorenkoPets = Set.of(simpleDomesticCat);
        Family sydorenkoFamily = new Family(sydorenkoMother,sydorenkoFather,sydorenkoChildren,sydorenkoPets);

        Man chumakFather = new Man();
        Woman chumakMother = new Woman("Mother","Chumak",1996);
        Human chumakChild1 = new Human("Child1","Chumak",2017);
        Human chumakChild2 = new Human();
        Human chumakChild3 = new Human("Child3", "Chumak", 2022);
        Family chumakFamily = new Family();


        List<Family> families = new ArrayList<>(List.of(
                petrenkoFamilyWithoutChildren,
                sydorenkoFamily,
                chumakFamily
        ));
        CollectionFamilyDao familyDao = new CollectionFamilyDao(families);
        module = new FamilyService(familyDao);
    }

    @Test
    public void getAllFamilies(){
        String actual = module.getAllFamilies().toString();
        String expected = "[Family{mother=Woman{name='Mother', surname='Petrenko', year=1979, iq=78, schedule={WEDNESDAY=Meditate, MONDAY=Get Enough Sleep, THURSDAY=Workout, SUNDAY=Take Breaks To Re-energize, TUESDAY=Rise Early, FRIDAY=Eat A Good Breakfast, SATURDAY=Take A Nap}}, father=Man{name='Father', surname='Petrenko', year=1974, iq=0, schedule=null}, children=null, pet=no record}, Family{mother=Woman{name='Mother', surname='Sydorenko', year=1979, iq=78, schedule={WEDNESDAY=Meditate, MONDAY=Get Enough Sleep, THURSDAY=Workout, SUNDAY=Take Breaks To Re-energize, TUESDAY=Rise Early, FRIDAY=Eat A Good Breakfast, SATURDAY=Take A Nap}}, father=Man{name='Father', surname='Sydorenko', year=1974, iq=0, schedule=null}, children=[Human{name='childBoy', surname='Sydorenko', year=2004, iq=0, schedule=null}], pet=[DOMESTIC_CAT{nickname='null', age=0, trickLevel=0, habits=null}]}, Family{mother=Woman{name='null', surname='null', year=0, iq=0, schedule=null}, father=Man{name='null', surname='null', year=0, iq=0, schedule=null}, children=null, pet=no record}]";
        assertEquals(expected, actual);
    }

    /*
    @Test
    void displayAllFamilies(){
        String expected = "";
        String consoleOutput = null;
        PrintStream originalOut = System.out;
        try {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream(200);
            PrintStream capture = new PrintStream(outputStream);
            System.setOut(capture);
            module.displayAllFamilies();
            capture.flush();
            consoleOutput = outputStream.toString();
            System.setOut(originalOut);

        } catch (Exception e){
            e.printStackTrace();
        }
        assertEquals(expected,consoleOutput);
    }

     */

    @Test
    void getFamiliesBiggerThan(){
        String actual = module.getFamiliesBiggerThan(3).toString();
        String  expected = "[Family{mother=Woman{name='Mother', surname='Sydorenko', year=1979, iq=78, schedule={WEDNESDAY=Meditate, MONDAY=Get Enough Sleep, THURSDAY=Workout, SUNDAY=Take Breaks To Re-energize, TUESDAY=Rise Early, FRIDAY=Eat A Good Breakfast, SATURDAY=Take A Nap}}, father=Man{name='Father', surname='Sydorenko', year=1974, iq=0, schedule=null}, children=[Human{name='childBoy', surname='Sydorenko', year=2004, iq=0, schedule=null}], pet=[DOMESTIC_CAT{nickname='null', age=0, trickLevel=0, habits=null}]}]";
        assertEquals(expected,actual);
    }

    @Test
    void getFamiliesLessThan(){
        String actual = module.getFamiliesLessThan(3).toString();
        String  expected = "[Family{mother=Woman{name='Mother', surname='Petrenko', year=1979, iq=78, schedule={WEDNESDAY=Meditate, MONDAY=Get Enough Sleep, THURSDAY=Workout, SUNDAY=Take Breaks To Re-energize, TUESDAY=Rise Early, FRIDAY=Eat A Good Breakfast, SATURDAY=Take A Nap}}, father=Man{name='Father', surname='Petrenko', year=1974, iq=0, schedule=null}, children=null, pet=no record}, Family{mother=Woman{name='null', surname='null', year=0, iq=0, schedule=null}, father=Man{name='null', surname='null', year=0, iq=0, schedule=null}, children=null, pet=no record}]";
        assertEquals(expected,actual);
    }

    @Test
    void countFamiliesWithMemberNumber(){
        int actual = module.countFamiliesWithMemberNumber(2);
        int  expected = 2;
        assertEquals(expected,actual);
    }

    @Test
    void deleteFamilyByIndex(){

    }

    /*
    @Test
    void bornChild(){
        String actual = module.bornChild(module.getFamilyById(0),"Ania","Dima").toString();
        String  expected = "Family{mother=Woman{name='Mother', surname='Petrenko', year=1979, iq=78, schedule={WEDNESDAY=Meditate, MONDAY=Get Enough Sleep, THURSDAY=Workout, SUNDAY=Take Breaks To Re-energize, TUESDAY=Rise Early, FRIDAY=Eat A Good Breakfast, SATURDAY=Take A Nap}}, father=Man{name='Father', surname='Petrenko', year=1974, iq=0, schedule=null}, children=[Human{name='Dima', surname='Petrenko', year=2023, iq=0, schedule=null}], pet=no record}";
        assertEquals(expected,actual);
    }
     */

    @Test
    void adoptChild() {
        AdoptiveChild newChild = new AdoptiveChild("Girl","Ivanenko","20/03/2015", 78);
        String actual = module.adoptChild(module.getFamilyById(0),newChild).toString();
        String  expected = "Family{mother=Woman{name='Mother', surname='Petrenko', year=1979, iq=78, schedule={WEDNESDAY=Meditate, MONDAY=Get Enough Sleep, THURSDAY=Workout, SUNDAY=Take Breaks To Re-energize, TUESDAY=Rise Early, FRIDAY=Eat A Good Breakfast, SATURDAY=Take A Nap}}, father=Man{name='Father', surname='Petrenko', year=1974, iq=0, schedule=null}, children=[Human{name='Girl', surname='Ivanenko', year=2015, iq=0, schedule=null}], pet=no record}";
        assertEquals(expected,actual);
    }

    @Test
    void deleteAllChildrenOlderThan(){
        module.deleteAllChildrenOlderThan(10);
        String actual = module.getAllFamilies().toString();
        String expected = "[Family{mother=Woman{name='Mother', surname='Petrenko', year=1979, iq=78, schedule={WEDNESDAY=Meditate, MONDAY=Get Enough Sleep, THURSDAY=Workout, SUNDAY=Take Breaks To Re-energize, TUESDAY=Rise Early, FRIDAY=Eat A Good Breakfast, SATURDAY=Take A Nap}}, father=Man{name='Father', surname='Petrenko', year=1974, iq=0, schedule=null}, children=null, pet=no record}, Family{mother=Woman{name='Mother', surname='Sydorenko', year=1979, iq=78, schedule={WEDNESDAY=Meditate, MONDAY=Get Enough Sleep, THURSDAY=Workout, SUNDAY=Take Breaks To Re-energize, TUESDAY=Rise Early, FRIDAY=Eat A Good Breakfast, SATURDAY=Take A Nap}}, father=Man{name='Father', surname='Sydorenko', year=1974, iq=0, schedule=null}, children=[Human{name='childBoy', surname='Sydorenko', year=2004, iq=0, schedule=null}], pet=[DOMESTIC_CAT{nickname='null', age=0, trickLevel=0, habits=null}]}, Family{mother=Woman{name='null', surname='null', year=0, iq=0, schedule=null}, father=Man{name='null', surname='null', year=0, iq=0, schedule=null}, children=null, pet=no record}]";
        assertEquals(expected,actual);
    }

    @Test
    void getFamilyById(){
        String actual = module.getFamilyById(0).toString();
        String expected = "Family{mother=Woman{name='Mother', surname='Petrenko', year=1979, iq=78, schedule={WEDNESDAY=Meditate, MONDAY=Get Enough Sleep, THURSDAY=Workout, SUNDAY=Take Breaks To Re-energize, TUESDAY=Rise Early, FRIDAY=Eat A Good Breakfast, SATURDAY=Take A Nap}}, father=Man{name='Father', surname='Petrenko', year=1974, iq=0, schedule=null}, children=null, pet=no record}";
        assertEquals(expected,actual);
    }

    @Test
    void getPets(){
        String actual = module.getPets(1).toString();
        String expected = "[DOMESTIC_CAT{nickname='null', age=0, trickLevel=0, habits=null}]";
        assertEquals(expected,actual);
    }

    @Test
    void addPet(){
        module.addPet(0, new Dog());
        String actual = module.getPets(0).toString();
        String expected = "[DOG{nickname='null', age=0, trickLevel=0, habits=null}]";
        assertEquals(expected,actual);
    }

}
