package HW9;

import java.util.Map;

public final class Man extends Human{
    public Man() {
    }

    public Man(String name, String surname, long birthDate) {
        super(name, surname, birthDate);
    }

    public Man(String name, String surname, long birthDate, int iq, Map<String,String> schedule) {
        super(name, surname, birthDate, iq, schedule);
    }

    @Override
    public void greetPets() {
        super.greetPets();
        System.out.println("Woman said");
    }

    public void repairCar(){
        System.out.println("Finally! I've finished repairing my car.");
    }


}
