package HW9;

import java.util.Set;

public class RoboCat extends Pet{
    public RoboCat() {
        setSpecies();
    }

    public RoboCat(String nickname) {
        super(nickname);
        setSpecies();
    }

    public RoboCat(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
        setSpecies();
    }
    @Override
    public Species getSpecies(){
        return this.species;
    }

    public void setSpecies(){
        this.species = Species.ROBOCAT;
    }

    @Override
    public void respond() {
        System.out.println("Hi, owner! I'm "+this.nickname+". I miss you.");
    }
}
