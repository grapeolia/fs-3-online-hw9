package HW9;

import java.util.Set;

public class Fish extends Pet {

    public Fish() {
        setSpecies();

    }

    public Fish(String nickname) {
        super(nickname);
        setSpecies();

    }

    public Fish(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
        setSpecies();

    }

    public void setSpecies() {
        this.species = Species.FISH;
    }
    @Override
    public Species getSpecies(){
        return this.species;
    }

    @Override
    public void respond() {
        System.out.println("Hi, owner! I'm "+this.nickname+". I miss you.");
    }
}
