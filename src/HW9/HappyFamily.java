package HW9;

import java.text.ParseException;
import java.util.*;

public class HappyFamily {
    public static void main(String[] args) {

        Set<String> petHabits = Set.of("eat","sleep","jump");
        Pet simpleDomesticCat = new DomesticCat();
        Pet fullDog = new Dog("Tofik",2,34,petHabits);

        Man petrenkoFather = new Man("Father", "Petrenko", -128088366000L);
        System.out.println(petrenkoFather);
        petrenkoFather.describeAge();
        Woman petrenkoMother = new Woman("Mother", "Petrenko", 128088366000L,78,setUpDailySchedule());
        Family petrenkoFamilyWithoutChildren = new Family(petrenkoMother,petrenkoFather);

        Man sydorenkoFather = new Man("Father", "Sydorenko", 128088366L);
        Woman sydorenkoMother = new Woman("Mother", "Sydorenko", 128088396000L,78,setUpDailySchedule());
        Human sydorenkoBoyChild = new Human("childBoy","Sydorenko",1086310680000L);
        List<Human> sydorenkoChildren = List.of(sydorenkoBoyChild);
        Set<Pet> sydorenkoPets = Set.of(simpleDomesticCat);
        Family sydorenkoFamily = new Family(sydorenkoMother,sydorenkoFather,sydorenkoChildren,sydorenkoPets);

        Man chumakFather = new Man();
        Woman chumakMother = new Woman("Mother","Chumak",850395480000L);
        Human chumakChild2 = new Human();
        Human chumakChild3 = new Human("Child3", "Chumak", 1650977880000L);
        Family chumakFamily = new Family();



        List<Family> families = new ArrayList<>(List.of(
                petrenkoFamilyWithoutChildren,
                sydorenkoFamily,
                chumakFamily
        ));
        CollectionFamilyDao familyDao = new CollectionFamilyDao(families);
        FamilyService familyService = new FamilyService(familyDao);
        FamilyController familyController = new FamilyController(familyService);

        familyController.start();

    }

    public static Map<String,String> setUpDailySchedule(){
        Map<String,String> schedule = new HashMap<>();
        schedule.put(DayOfWeek.MONDAY.name(),"Get Enough Sleep");
        schedule.put(DayOfWeek.TUESDAY.name(),"Rise Early");
        schedule.put(DayOfWeek.WEDNESDAY.name(),"Meditate");
        schedule.put(DayOfWeek.THURSDAY.name(), "Workout");
        schedule.put(DayOfWeek.FRIDAY.name(),"Eat A Good Breakfast");
        schedule.put(DayOfWeek.SATURDAY.name(),"Take A Nap");
        schedule.put(DayOfWeek.SUNDAY.name(), "Take Breaks To Re-energize");

        return schedule;
    }

}