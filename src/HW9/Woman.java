package HW9;

import java.util.Map;

public final class Woman extends Human{
    public Woman() {
    }

    public Woman(String name, String surname, long birthDate) {
        super(name, surname, birthDate);
    }

    public Woman(String name, String surname, long birthDate, int iq, Map<String,String> schedule) {
        super(name, surname, birthDate, iq, schedule);
    }

    @Override
    public void greetPets() {
        super.greetPets();
        System.out.println("Woman said");
    }

    public void makeup(){
        System.out.println("I've finished make up. I'm beautiful!");
    }

}
