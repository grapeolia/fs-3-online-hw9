package HW9;

import java.time.Year;
import java.util.*;

public class FamilyService {
    private final CollectionFamilyDao collectionFamilyDao;

    public FamilyService(CollectionFamilyDao collectionFamilyDao) {
        this.collectionFamilyDao = collectionFamilyDao;
    }

    public List<Family> getAllFamilies (){
        return collectionFamilyDao.getAllFamilies();
    }

    public void displayAllFamilies(){
        if(getAllFamilies() != null){
            getAllFamilies()
                    .forEach(family -> System.out.println("["+getAllFamilies().indexOf(family)+"]"+" "+family));
        }
    }
    public List<Family> getFamiliesBiggerThan(int minFamilySize){
        List<Family> familiesBiggerThan = new ArrayList<>();
        if(getAllFamilies() != null){
            getAllFamilies()
                    .stream()
                    .filter(family -> family.countFamilyMembers()>minFamilySize)
                    .forEach(familiesBiggerThan::add);
        }
        return familiesBiggerThan;
    }

    public List<Family> getFamiliesLessThan(int maxFamilySize){
        List<Family> familiesBiggerThan = new ArrayList<>();
        if(getAllFamilies() != null){
            getAllFamilies()
                    .stream()
                    .filter(family -> family.countFamilyMembers()<maxFamilySize)
                    .forEach(familiesBiggerThan::add);
        }
        return familiesBiggerThan;
    }

    public int countFamiliesWithMemberNumber(int familySize){
        List<Family> familiesWithExactNumbers = new ArrayList<>();
        if(getAllFamilies() != null){
            getAllFamilies()
                    .stream()
                    .filter(family -> family.countFamilyMembers() == familySize)
                    .forEach(familiesWithExactNumbers::add);
                    }
        return familiesWithExactNumbers.size();
    }

    public void createNewFamily(Woman mother, Man father){
        collectionFamilyDao.saveFamily(new Family(mother,father));
        System.out.println("The family has been created and added to families");
    }

    public void deleteFamilyByIndex(int familyIndex){
        boolean isDeleted = collectionFamilyDao.deleteFamily(familyIndex);
        if(isDeleted) {
            System.out.println("The family has been deleted");
        }else {
            System.out.println("Such family doesn't exist");
        }
    }

    public Family bornChild(Family familyForChild, String femaleName, String maleName) {
        Random rand = new Random();
        int randomValueToDefineSex = rand.nextInt(2);
        String childSex = randomValueToDefineSex != 0 ? "male" : "female";
        String childName = childSex.equals("male") ? maleName : femaleName;
        int currentYear = Year.now().getValue();
        String childSurname;
        String fathersSurname = familyForChild.getFather().getSurname();
        String mothersSurname = familyForChild.getMother().getSurname();
        childSurname = Objects.requireNonNullElseGet(fathersSurname, () -> Objects.requireNonNullElse(mothersSurname, "no surname from parent"));
        Human newChild = new Human(childName,childSurname,currentYear);
        collectionFamilyDao.addChild(familyForChild,newChild);
        return familyForChild;
    }

    public Family adoptChild(Family familyForChild, AdoptiveChild newChild){
        collectionFamilyDao.addChild(familyForChild,newChild);
        return familyForChild;
    }

    public int count(){
        return collectionFamilyDao.count();
    }

    public void deleteAllChildrenOlderThan(int minAge){
        collectionFamilyDao.deleteAllChildrenOlderThan(minAge);
    }

    public Family getFamilyById(int familyIndex){
        return collectionFamilyDao.getFamilyByIndex(familyIndex);
    }

    public Set<Pet> getPets(int familyIndex){
        return collectionFamilyDao.getPets(familyIndex);
    }

    public void addPet(int familyIndex, Pet newPet){
        collectionFamilyDao.addPet(familyIndex,newPet);
    }

}
